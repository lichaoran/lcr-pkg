import { defineConfig } from 'vite'
import { fileURLToPath, URL } from 'url'
import react from '@vitejs/plugin-react'

export default defineConfig({
  build: {
    lib: {
      entry: 'lib/index.ts',
      name: 'index',
    },
  },
  resolve: {
    alias: {
      '@styles': fileURLToPath(new URL('./packages/styles', import.meta.url)),
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        // 引入全局 minxin 和 variable
        additionalData: `
          @import "@styles/mixin.scss";
          @import "@styles/variable.scss";
        `,
      },
    },
  },
  plugins: [react()],
})
