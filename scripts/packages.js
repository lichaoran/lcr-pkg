import fs from 'fs'
import path from 'path'
import { fileURLToPath } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// packages 文件目录
const packagesRoot = path.resolve(__dirname, '../packages')
// 不打包的文件
const ignoreFile = ['styles']

export const packages = fs
  .readdirSync(packagesRoot)
  // 过滤不打包的文件，只保留文件夹
  .filter(item => {
    return fs.statSync(path.resolve(packagesRoot, item)).isDirectory() && !ignoreFile.includes(item)
  })
