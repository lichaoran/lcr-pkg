import shell from 'shelljs'
import inquirer from 'inquirer'
import { packages } from './packages.js'

async function choosePackages() {
  const result = await inquirer.prompt({
    name: 'packages',
    type: 'checkbox',
    message: '请选择需要打包的包名，按回车键确认（<空格> 选择, <a> 全选, <i> 反选）',
    choices: packages,
  })

  const filter = result.packages.reduce((filterStr, item) => filterStr + ` --filter ${item}`, '')

  shell.exec(`turbo run build ${filter}`)
}

choosePackages()
